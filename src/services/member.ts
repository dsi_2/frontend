import type { member } from '@/types/Member'
import http from './http'

function addMember(member: member) {
  return http.post('/member', member)
}
function updateMember(member: member) {
  return http.patch(`/member/${member.id}`, member)
}

function delMember(member: member) {
  return http.delete(`/member/${member.id}`)
}

function delMemberId(id: number) {
  return http.delete(`/member/${id}`)
}

function getMember(id: number) {
  return http.get(`/member/${id}`)
}

function getMembers() {
  return http.get('/member')
}

export default { addMember, updateMember, delMember, getMember, getMembers, delMemberId }
