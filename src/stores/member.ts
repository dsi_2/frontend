import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { member } from '@/types/Member'
import MemberService from '@/services/member'
import { useLoadingStore } from './loading'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const delmemberid = ref(0)
  const members = ref<member[]>([
    // {
    //   id: 1,
    //   Fullname: 'Rick',
    //   Tel: '0855122249',
    //   Point: 10,
    // },
    // {
    //   id: 2,
    //   Fullname: 'Morty',
    //   Tel: '0862475512',
    //   Point: 1,
    // },
    // {
    //   id: 3,
    //   Fullname: 'Cloud',
    //   Tel: '0877655312',
    //   Point: 0,
    // }
  ])

  const currentMember = ref<member | null>()
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.Tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }

  const updatePoint = (newPoint: number) => {
    if (currentMember.value) {
      currentMember.value.Point = newPoint
    }
  }

  function clear() {
    currentMember.value = null
  }

  const memberDialog = ref(false)
  const dialogDelete = ref(false)
  const editIndex = -1
  function newCustomer() {
    memberDialog.value = true
  }

  const createNewMember = (newMember: member) => {
    members.value.push(newMember)
  }

  const initialCustomer: member = {
    Fullname: 'Rick',
    Tel: '0000000000',
    Point: 0
  }

  const editedCustomer = ref<member>(JSON.parse(JSON.stringify(initialCustomer)))

  async function initCustomer() {
    const initmember: member = {
      Fullname: 'Rick',
      Tel: '0855122249',
      Point: 10
    }
    // await MemberService.addMember(initmember);
  }

  async function getMember(id: number) {
    loadingStore.doLoad()
    const res = await MemberService.getMember(id)
    members.value = res.data
    loadingStore.finish()
  }

  async function getMembers() {
    loadingStore.doLoad()
    const res = await MemberService.getMembers()
    members.value = res.data
    loadingStore.finish()
  }

  async function saveMember(member: member) {
    loadingStore.doLoad()
    if (!member.id) {
      console.log('Post' + JSON.stringify(member))
      //add new
      const res = await MemberService.addMember(member)
    } else {
      console.log('Patch' + JSON.stringify(member))
      //update
      const res = await MemberService.updateMember(member)
    }
    await getMembers()
    editedCustomer.value = { ...initialCustomer }
    loadingStore.finish()
  }

  async function deleteMember(member: member) {
    loadingStore.doLoad()
    const res = await MemberService.delMember(member)
    await getMembers()
    loadingStore.finish()
  }

  async function deleteMemberbyID(id: number) {
    loadingStore.doLoad()
    const res = await MemberService.delMemberId(id)
    await getMembers()
    loadingStore.finish()
  }

  return {
    members,
    currentMember,
    memberDialog,
    editedCustomer,
    editIndex,
    dialogDelete,
    delmemberid,
    initCustomer,
    searchMember,
    clear,
    updatePoint,
    newCustomer,
    createNewMember,
    getMember,
    getMembers,
    saveMember,
    deleteMember,
    deleteMemberbyID
  }
})
