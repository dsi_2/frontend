import type { Employee } from '@/types/Employee'
import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useEmployeeStore = defineStore('employee', () => {
  const employees = ref<Employee[]>([
    {
      id: 1,
      name: 'Peerada Wangyaichim',
      email: 'Peerada@gmail.com',
      password: 'password',
      roles: ['employee'],
      gender: 'female',
      salaryType: 'Full-Time',
      salary: 23000,
      phone: '081',
      wagePerHour: 143.75,
      branch_id: [2]
    },
    {
      id: 2,
      name: 'Wanwadee Noijaroen',
      email: 'Wanwadee@gmail.com',
      password: 'password',
      roles: ['manager', 'employee'],
      gender: 'female',
      salaryType: 'Full-Time',
      salary: 22500,
      phone: '082',
      wagePerHour: 140.625,
      branch_id: [2]
    },
    {
      id: 3,
      name: 'Tanakorn Pummala',
      email: 'Tanakorn@gmail.com',
      password: 'password',
      roles: ['employee'],
      gender: 'male',
      salaryType: 'Part-Time',
      phone: '083',
      wagePerHour: 52.5,
      branch_id: [2]
    }
  ])

  const employeeDialog = ref(false)

  return { employees, employeeDialog }
})
