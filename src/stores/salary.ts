import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Salary } from '@/types/salary'

export const useSalaryStore = defineStore('salary', () => {
  const salaries = ref<Salary[]>([
    {
      id: 1,
      workTimmeID: 1,
      employeeID: 1,
      salaryCreate: new Date(),
      salaryPay: new Date(),
      salaryTotal: 30000,
      salaryStatus: 'Paid'
    },
    {
      id: 2,
      workTimmeID: 2,
      employeeID: 2,
      salaryCreate: new Date(),
      salaryPay: new Date(),
      salaryTotal: 30000,
      salaryStatus: 'Paid'
    },
    {
      id: 3,
      workTimmeID: 3,
      employeeID: 3,
      salaryCreate: new Date(),
      salaryPay: new Date(),
      salaryTotal: 15000,
      salaryStatus: 'Paid'
    },
    {
      id: 4,
      workTimmeID: 4,
      employeeID: 4,
      salaryCreate: new Date(),
      salaryTotal: 15000,
      salaryStatus: 'Not Paid'
    },
    {
      id: 5,
      workTimmeID: 5,
      employeeID: 5,
      salaryCreate: new Date(),
      salaryTotal: 9900,
      salaryStatus: 'Not Paid'
    }
  ])

  return { salaries }
})
