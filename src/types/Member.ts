type member = {
  id?: number
  Fullname: string
  Tel: string
  Point: number
  // deleted?: boolean;
}

export type { member }
