type Salary = {
  id: number
  workTimmeID: number
  employeeID: number
  salaryCreate: Date
  salaryPay?: Date
  salaryTotal: number
  salaryStatus: 'Paid' | 'Not Paid'
}

export { type Salary }
