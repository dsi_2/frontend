type Employee = {
  id: number
  name: string
  email: string
  password: string
  roles: ('owner' | 'manager' | 'employee')[]
  gender: 'male' | 'female' | 'others'
  salaryType: 'Full-Time' | 'Part-Time'
  salary?: number
  phone: string
  wagePerHour: number
  branch_id: number[]
}

export { type Employee }
